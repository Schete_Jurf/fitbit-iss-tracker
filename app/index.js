/*
 * Stefanie Jung 09.07.2020
 */

//---------------Imports---------------
import document from "document";
import { vibration } from "haptics";
import * as messaging from "messaging";

//---------------Variables---------------

let issLatitude = document.getElementById("issLatitude");
let issLongitude = document.getElementById("issLongitude");
let riseTime = document.getElementById("riseTime");
let duration = document.getElementById("duration");
let compass = document.getElementById("compass");

var issPosition=document.getElementById("issPosition");
var issLocation=document.getElementById("issLocation");
issLocation.style.display="none";
var issTime=document.getElementById("issTime");

let status = document.getElementById("status");

let issImage=document.getElementById("issImage");
let line=document.getElementById("line");

var phoneTimeout;

issImage.style.display="inline";
status.text="Connecting to phone..."



//---------------Processing Functions---------------
phoneTimeout = setTimeout(timeoutOver,15000);

function timeoutOver(){
  vibration.start("bump");
  processStatus("No connection to phone");
}

function processStatus(data){
  status.text=data;
}

function processLocation(data){
  issLocation.style.display="inline";
  issLocation.text=data;
}

function processIssLocationData(data){
  vibration.start("confirmation");
  line.style.display="inline";
  issImage.style.display="none";
  //status.style.display="none";
  issPosition.text="ISS Location:";
  issLatitude.text="Lat: "+data.issLatitude;
  issLongitude.text="Long: "+data.issLongitude;

}
function processIssPassData(data){
  issImage.style.display="none";
  status.style.display="none";
  issTime.text="Next pass at your position:";
  riseTime.text=data.riseTime+" UTC";
  duration.text="Duration: "+data.duration+" seconds";
  compass.text='From "'+data.from+'" to "'+data.to+'"';
}

//---------------Message Functions---------------

function fetchData(){
    if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    // Send a command to the companion
    messaging.peerSocket.send({
      command: 'data'
    });
  }
}

// Listen for the onopen event
messaging.peerSocket.onopen = function() {
  // Fetch location when the connection opens
  fetchData();
}

// Listen for messages from the companion
messaging.peerSocket.onmessage = function(evt) {
  if (evt.data) {
    if (evt.data.issLatitude !=undefined){
      processIssLocationData(evt.data);
    }else if(evt.data.riseTime !=undefined){
      processIssPassData(evt.data);       
    }else if(evt.data=="Searching position..."||evt.data=="Calculating ISS data..."){
      clearTimeout(phoneTimeout);
      processStatus(evt.data);
    }else if(evt.data=="No connection to internet"||evt.data=="No GPS connection"){
      vibration.start("bump");
      processStatus(evt.data);
    }else if(evt.data.location!=undefined){
      processLocation(evt.data.location);
    }
  }
}

// Listen for the onerror event
messaging.peerSocket.onerror = function(err) {
  // Handle any errors
  timeoutOver();
}