import { geolocation } from "geolocation";
import * as messaging from "messaging";
import * as util from "../common/utils";

const APIKEY_n2yo="YOURAPIKEY";
const API_KEY_w3w="YOURAPIKEY";

var userLatitude;
var userLongitude;
var userAltitude;

var status;

function getLocation(){
  
  status="Searching position...";
  returnData(status);
  
  
  geolocation.getCurrentPosition(locationSuccess, locationError, {
    timeout: 20 * 1000
  });
}

function locationSuccess(position) {
  status="Calculating ISS data...";
  returnData(status);
  
  userLatitude = position.coords.latitude;
  userLongitude= position.coords.longitude;
  userAltitude = position.coords.altitude;
  
  //retrieving ISS Data
  let url = 'https://www.n2yo.com/rest/v1/satellite/visualpasses/25544/'+userLatitude+'/'+userLongitude+'/'+userAltitude+'/5/30&apiKey='+APIKEY_n2yo;
  fetch(url).then(function(response) {
      return response.json();
  }).then(function(data) {
      var issPassData={
          riseTime: data.passes[0].startUTC,
          duration: data.passes[0].duration,
          from: data.passes[0].startAzCompass,
          to: data.passes[0].endAzCompass
        }
      var issControlData={
            passes: parseInt(data.info.passescount)
            }
      if (issControlData.passes>0){
        issPassData.riseTime=timeConverter(issPassData.riseTime);
        returnData(issPassData);
      }else{
        issPassData.riseTime="no pass in next";
        issPassData.duration="5 days";
        returnData(issPassData);
      }
     });
  
 }

function whereIsISS(){
  var internetTimeout=setTimeout(timeoutOver,22000);
  let url='https://www.n2yo.com/rest/v1/satellite/positions/25544/'+userLatitude+'/'+userLongitude+'/'+userAltitude+'/1/&apiKey='+APIKEY_n2yo;
    fetch(url).then(function(response) {
      return response.json();
  }).then(function(data) {
      clearTimeout(internetTimeout);
      var issLocationData={
          issLatitude: data.positions[0].satlatitude,
          issLongitude: data.positions[0].satlongitude
        }
      returnData(issLocationData); 
      issPotistion(issLocationData);
      } 
     );
}

function issPotistion(data){
      //retrieving what3words
  let urlw3w = 'https://api.what3words.com/v3/convert-to-3wa?coordinates=' + data.issLatitude + '%2C' + data.issLongitude + '&key='+API_KEY_w3w;
  fetch(urlw3w).then(function(response) {
      return response.json();
  }).then(function(data) {
    var locationData={
        location: data.nearestPlace,
      }
      returnData(locationData);
     });
}
          
function locationError(error){
  status="No GPS connection";
  returnData(status);
}

//---------------Message Functions---------------
function returnData(data){
    if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    // Send a command to the device
    messaging.peerSocket.send(data);
  } else {
    //console.log("Error: Connection is not open");
  }
}

// Listen for messages from the device
messaging.peerSocket.onmessage = function(evt) {
  if (evt.data && evt.data.command == "data") {
    // The device requested location data
    whereIsISS();
    getLocation();
  }
}

// Listen for the onerror event
messaging.peerSocket.onerror = function(err) {
  // Handle any errors
  function locationError(error)
}

//---------------Helper Functions---------------
function timeoutOver(){
  status="No connection to internet";
  returnData(status);
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = util.zeroPad(a.getUTCDate());
  var hour = util.zeroPad(a.getUTCHours());
  var min = util.zeroPad(a.getUTCMinutes());
  //var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min;
  return time;
}