Description from Fitbit Appgallery:

Displays current location of ISS and time of next flyover at your position.

Shows nearest place if availbale.

Uses https://www.n2yo.com API

Icon by Freepik https://www.flaticon.com/authors/freepik

Check out my other apps: https://gallery.fitbit.com/developer/9906fd2b-5960-4b2a-8dc6-fe7854c91efe

![animated iss](/images/ezgif.com-gif-to-apng.png)
![iss data](/images/ISS-screenshot2.png)
